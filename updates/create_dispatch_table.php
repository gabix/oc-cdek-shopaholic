<?php namespace Gabix\CdekShopaholic\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class CreateDispatchTable extends Migration
{
    const TABLE_NAME = 'gabix_cdek_shopaholic_dispatch';

    public function up()
    {
        if (Schema::hasTable(self::TABLE_NAME)) {
            return;
        }

        Schema::create(self::TABLE_NAME, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('order_id')->unsigned()->unique();
            $table->integer('city_id')->unsigned()->defaultNull()->index();
            $table->integer('tariff_id')->unsigned()->defaultNull();
            $table->string('pvz_code', 30)->nullable()->defaultNull();
            $table->unsignedInteger('region_id')->nullable()->defaultNull()->index();
            $table->unsignedInteger('country_id')->nullable()->defaultNull()->index();
            $table->string('country_iso', 3)->nullable()->defaultNull();
            $table->text('city_data')->nullable()->defaultNull();
            $table->boolean('sms_notify')->nullable()->default(0);
            $table->string('dispatch_number', 30)->nullable()->defaultNull();
            $table->dateTime('sent_date')->nullable()->defaultNull();
            $table->dateTime('modify_date')->nullable()->defaultNull();
            $table->smallInteger('status_code')->nullable()->defaultNull();
            $table->string('status_city', 120)->nullable()->defaultNull();
            $table->dateTime('status_date')->nullable()->defaultNull();
        });
    }

    public function down()
    {
        Schema::dropIfExists(self::TABLE_NAME);
    }

}
