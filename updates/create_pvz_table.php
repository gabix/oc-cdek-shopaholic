<?php namespace Gabix\CdekShopaholic\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class CreatePvzTable extends Migration
{
    const TABLE_NAME = 'gabix_cdek_shopaholic_pvz';

    public function up()
    {
        if (Schema::hasTable(self::TABLE_NAME)) {
            return;
        }

        Schema::create(self::TABLE_NAME, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('code', 30);
            $table->string('address', 191)->nullable()->defaultNull();
            $table->text('address_comment')->nullable()->defaultNull();
            $table->boolean('allowed_cod')->nullable()->defaultNull();
            $table->string('city', 120)->nullable()->defaultNull();
            $table->integer('city_code')->unsigned()->nullable()->defaultNull()->index();
            $table->integer('country_code')->unsigned()->nullable()->defaultNull()->index();
            $table->string('country_code_iso', 12)->nullable()->defaultNull();
            $table->string('country_name', 120)->nullable()->defaultNull();
            $table->string('email', 120)->nullable()->defaultNull();
            $table->string('full_address', 191)->nullable()->defaultNull();
            $table->boolean('have_cashless')->nullable()->defaultNull();
            $table->boolean('is_dressing_room')->nullable()->defaultNull();
            $table->string('metro_station', 120)->nullable()->defaultNull();
            $table->string('name', 120)->nullable()->defaultNull();
            $table->text('nearest_station')->nullable()->defaultNull();
            $table->text('note')->nullable()->defaultNull();
            $table->text('office_images')->nullable()->defaultNull();
            $table->string('phone', 120)->nullable()->defaultNull();
            $table->string('postal_code', 20)->nullable()->defaultNull();
            $table->integer('region_code')->unsigned()->nullable()->defaultNull();
            $table->string('region_name', 120)->nullable()->defaultNull();
            $table->string('site', 190)->nullable()->defaultNull();
            $table->string('type', 60)->nullable()->defaultNull()->index();
            $table->text('weight_limit')->nullable()->defaultNull();
            $table->string('work_time', 190)->nullable()->defaultNull();
            $table->double('coord_x', 11, 8)->nullable()->defaultNull();
            $table->double('coord_y', 11, 8)->nullable()->defaultNull();
            $table->text('phone_details')->nullable()->defaultNull();
            $table->text('work_times')->nullable()->defaultNull();
            $table->smallInteger('active')->default(1)->index();
            $table->timestamps();
            $table->primary('code');
        });
    }

    public function down()
    {
        Schema::dropIfExists(self::TABLE_NAME);
    }
}