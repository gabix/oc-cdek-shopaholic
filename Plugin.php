<?php namespace Gabix\CdekShopaholic;

use System\Classes\PluginBase;
use Event;
use Gabix\CdekShopaholic\Classes\Event\OrdersHandler;
use Gabix\CdekShopaholic\Console\CdekPvzCommand;
use Gabix\CdekShopaholic\Console\CdekStatusCommand;
use Gabix\CdekShopaholic\Models\Settings;

/**
 * Class Plugin
 *
 * @package Gabix\CdekShopaholic
 */
class Plugin extends PluginBase
{
    /** @var array Plugin dependencies */
    public $require = [
        'Lovata.Toolbox',
        'Lovata.Shopaholic',
        'Lovata.OrdersShopaholic'
    ];

    /**
     * @inheritdoc
     */
    public function pluginDetails()
    {
        return [
            'name' => 'gabix.cdekshopaholic::lang.plugin.name',
            'description' => 'gabix.cdekshopaholic::lang.plugin.description',
            'author' => 'NickolayCh',
            'icon' => 'icon-truck'
        ];
    }

    /**
     * @inheritdoc
     */
    public function registerComponents()
    {
        return [
            '\Gabix\CdekShopaholic\Components\CdekShipping' => 'CdekShipping',
        ];
    }

    /**
     * Plugin boot method
     */
    public function boot()
    {
        $this->addEventListener();
    }

    /**
     * Register method, called when the plugin is first registered.
     */
    public function register()
    {
        $this->registerConsoleCommand('shopaholic:cdek.pvz', CdekPvzCommand::class);
        $this->registerConsoleCommand('shopaholic:cdek.status', CdekStatusCommand::class);
    }

    /**
     * Add event listeners
     */
    protected function addEventListener()
    {
        Event::subscribe(OrdersHandler::class);
    }

    /**
     * @inheritdoc
     */
    public function registerPermissions()
    {
        return [
            'gabix.cdekshopaholic.access_settings' => [
                'tab'   => 'lovata.shopaholic::lang.tab.permissions',
                'label' => 'gabix.cdekshopaholic::lang.plugin.settings_description'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function registerSettings()
    {
        return [
            'settings' => [
                'label' => 'gabix.cdekshopaholic::lang.plugin.settings',
                'description' => 'gabix.cdekshopaholic::lang.plugin.settings_description',
                'category' => 'lovata.shopaholic::lang.tab.settings',
                'icon' => 'icon-truck',
                'class' => Settings::class,
                'order' => 500,
                'keywords' => 'shipping cdek',
                'permissions' => ['gabix.cdekshopaholic.access_settings']
            ]
        ];
    }
}
