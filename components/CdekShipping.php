<?php namespace Gabix\CdekShopaholic\Components;

use Cms\Classes\ComponentBase;
use Gabix\CdekShopaholic\Classes\Helper\CdekHelper;
use Kharanenka\Helper\Result;
use Input;
use Session;

/**
 * Class CdekShipping
 *
 * @package Gabix\CdekShopaholic\Components
 */
class CdekShipping extends ComponentBase
{
    /** @var array */
    protected $cityData;

    /** @var CdekHelper */
    protected $apiHelper;

    /**
     * @inheritdoc
     */
    public function componentDetails()
    {
        return [
            'name' => 'gabix.cdekshopaholic::lang.plugin.component',
        ];
    }

    /**
     * Init start data
     */
    public function init()
    {
        $this->apiHelper = CdekHelper::instance();
    }

    /**
     * Update city (ajax request)
     *
     * @return array
     */
    public function onCityUpdate()
    {
        $iShippingTypeID = Input::get('shipping_type_id');

        if (empty($iShippingTypeID)) {
            $sMessage = \Lang::get('lovata.toolbox::lang.message.e_not_correct_request');
            Result::setFalse()->setMessage($sMessage);
            return Result::get();
        }

        $pvzOffices = $this->getPvzOffices();

        Result::setData([
            'pvzOffices' => $pvzOffices
        ]);

        return Result::get();
    }

    /**
     * Ajax listener
     *
     * @return bool
     */
    public function onAjaxRequest()
    {
        return true;
    }

    /**
     * Return city data array
     *
     * @param null|string $param
     *
     * @return array
     */
    public function getCityData($param = null)
    {
        if (!isset($this->cityData)) {
            $this->cityData = [];
            $sessionData = (array) Session::get('cdek_city_data', []);
            $postData = (array) post('city_data', $sessionData);
            if ($postData) {
                $this->cityData = $this->apiHelper->getCityData($postData);
            }
            Session::put('cdek_city_data', $this->cityData);
        }

        if ($param) {
            return array_get($this->cityData, $param);
        }

        return $this->cityData;
    }

    /**
     * Return pvz offices array
     *
     * @return array
     */
    public function getPvzOffices()
    {
        $cityId = (int) $this->getCityData('id');

        return $this->apiHelper->getPvzList($cityId);
    }
}
