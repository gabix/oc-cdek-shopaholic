<?php namespace Gabix\CdekShopaholic\Console;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Gabix\CdekShopaholic\Classes\Helper\CdekHelper;
use Gabix\CdekShopaholic\Models\Settings;
use Gabix\CdekShopaholic\Models\CdekDispatch;
use CdekSDK\Requests;
use CdekSDK\Common;

/**
 * Class CdekStatusCommand
 *
 * @package Gabix\CdekShopaholic\Console
 */
class CdekStatusCommand extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'shopaholic:cdek.status';

    /**
     * @var string The console command description.
     */
    protected $description = 'Cdek orders status update';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shopaholic:cdek.status';

    /**
     * Counters data
     *
     * @var array
     */
    protected $counters = [
        'updated' => 0,
        'skipped' => 0,
        'errors' => 0,
    ];

    /**
     * Dispatch data
     *
     * @var CdekDispatch[]
     */
    protected $dispatches;

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->output->writeln('Starting update!');

        $lastStatusReportTime = (int) Settings::get('last_status_report_time');

        $client = CdekHelper::instance()->getClient();
        $request = new Requests\StatusReportRequest();

        if($lastStatusReportTime) {
            $startPeriod = Carbon::createFromTimestamp($lastStatusReportTime);
        } else {
            $startPeriod = Carbon::make('-1 day');
        }
        $endPeriod = Carbon::make('+1 day');

        // save last report update time
        Settings::set('last_status_report_time', time());

        $changePeriod = new Common\ChangePeriod($startPeriod, $endPeriod);
        $request->setChangePeriod($changePeriod);
        $request->setShowHistory(false);
        $response = $client->sendStatusReportRequest($request);

        if ($response->hasErrors()) {
            foreach ($response->getMessages() as $message) {
                $this->output->error($message);
            }
            return;
        }

        $dispatchNumbers = [];

        foreach ($response as $order) {
            $dispatchNumbers[] = $order->getDispatchNumber();
        }

        if (!$dispatchNumbers) {
            $this->output->writeln('Nothing to update!');
            return;
        }

        $this->loadDispatchData($dispatchNumbers);

        $this->processResponse($response);

        foreach ($this->counters as $label => $counter) {
            $this->output->writeln(ucfirst($label) . ': ' . $counter);
        }

    }

    /**
     * Process data
     *
     * @param $response \CdekSDK\Responses\StatusReportResponse|\CdekSDK\Common\Order[]
     */
    protected function processResponse($response)
    {
        foreach ($response as $order) {
            /** @var Common\Order $order */
            $dispatchNumber = $order->getDispatchNumber();
            $status = $order->getStatus();
            /** @var CdekDispatch $dispatch */
            $dispatch = $this->dispatches[$dispatchNumber] ?? null;
            if (!$dispatch || $dispatch->status_code == $status->getCode()) {
                ++$this->counters['skipped'];
                continue;
            }

            try {
                $dispatch->status_code = $status->getCode();
                $dispatch->status_city = $status->getCode();
                $dispatch->status_date = Carbon::now();
                $dispatch->save();
                $final_order_status_id = (int) Settings::get('final_order_status_id');
                if ($final_order_status_id && $status->isFinal() && $dispatch->order) {
                    // update order status
                    $dispatch->order->status_id = $final_order_status_id;
                    $dispatch->order->save();
                }
                ++$this->counters['updated'];
            } catch (\Exception $e) {
                ++$this->counters['errors'];
                $this->output->error('Error updating dispatch #'
                    . $dispatchNumber . ':' . $e->getMessage());
            }
        }
    }

    /**
     * Get data from DB
     *
     * @param array $dispatchNumbers
     */
    protected function loadDispatchData(array $dispatchNumbers)
    {
        $this->dispatches = CdekDispatch::whereIn('dispatch_number', $dispatchNumbers)
            ->with('order')->get()->keyBy('dispatch_number');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}