<?php namespace Gabix\CdekShopaholic\Console;

use Illuminate\Console\Command;
use Gabix\CdekShopaholic\Classes\Helper\CdekHelper;
use Gabix\CdekShopaholic\Models\Pvz;
use CdekSDK\Requests;
use Illuminate\Support\Collection;

/**
 * Class CdekPvzCommand
 *
 * @package Gabix\CdekShopaholic\Console
 */
class CdekPvzCommand extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'shopaholic:cdek.pvz';

    /**
     * @var string The console command description.
     */
    protected $description = 'Cdek pvz data update';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shopaholic:cdek.pvz';

    /**
     * Existing pvz offices
     *
     * @var Collection|Pvz[]
     */
    protected $offices;

    /**
     * Counters data
     *
     * @var array
     */
    protected $counters = [
        'created'     => 0,
        'updated'     => 0,
        'skipped'     => 0,
        'unpublished' => 0,
        'errors'      => 0,
    ];

    /**
     * Active codes
     *
     * @var array
     */
    protected $activeCodes = [
        'old'        => [],
        'new'        => []
    ];

    /** DocParser.php
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->output->writeln('Starting import!');

        $client = CdekHelper::instance()->getClient();

        $request = new Requests\PvzListRequest();
        $request->setType(Requests\PvzListRequest::TYPE_ALL);

        try {
            $response = $client->sendPvzListRequest($request);
        } catch (\Exception $e) {
            $this->output->writeln('Error: '.$e->getMessage());
            $response = false;
        }

        if ($response && !$response->hasErrors()) {
            $response = (array)$response->getItems();
        } else {
            $response = [];
        }

        if (!$response) {
            $this->output->writeln('Empty pvz list!');
            return;
        }

        $this->loadOffices();
        $this->processResponse($response);
        $this->deactivateOffices();

        foreach ($this->counters as $label => $counter) {
            $this->output->writeln(ucfirst($label) . ': ' . $counter);
        }

    }

    /**
     * Process data
     *
     * @param $response \CdekSDK\Responses\PvzListResponse|\CdekSDK\Common\Pvz[]
     */
    protected function processResponse($response)
    {
        foreach ($response as $office) {
            /** @var \CdekSDK\Common\Pvz $office */
            if (!empty($this->offices[$office->Code])) {
                $this->activeCodes['new'][] = $office->Code;
            }
            $model = $this->offices[$office->Code] ?? new Pvz();
            try {
                foreach ($office as $key => $value) {
                    $key = $key == 'CountryCodeISO' ? 'country_code_iso' : snake_case($key);
                    if ($model->isFillable($key)) {
                        $model->setAttribute($key, $value);
                    }
                }
            } catch (\Exception $e) {
                $this->output->writeln($office->Code . ' fill error: ' . $e->getMessage());
                continue;
            }
            $model->active = true;
            if (empty($this->offices[$office->Code]) || $model->isDirty()) {
                try {
                    $model->save();
                    $counterType = empty($this->offices[$office->Code]) ? 'created' : 'updated';
                    ++$this->counters[$counterType];
                    $this->offices[$office->Code] = $model;
                } catch (\Exception $e) {
                    ++$this->counters['errors'];
                    $this->output->writeln($office->Code . ' save error: ' . $e->getMessage());
                    continue;
                }
            } else {
                ++$this->counters['skipped'];
            }
        }
    }

    /**
     * Deactivate missing offices
     */
    protected function deactivateOffices()
    {
        $deactivate = array_diff($this->activeCodes['old'], $this->activeCodes['new']);

        if ($deactivate) {
            try {
                Pvz::where(['code' => $deactivate])->update(['active' => 0]);
                $this->counters['unpublished'] += count($deactivate);
            } catch (\Exception $e) {
                $this->output->writeln('Error deactivating: ' . $e->getMessage());
            }
        }
    }

    /**
     * Load db data
     */
    protected function loadOffices()
    {
        $this->offices = Pvz::all()->keyBy('code');

        foreach ($this->offices as $office) {
            if ($office->active) {
                $this->activeCodes['old'][] = $office->code;
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
