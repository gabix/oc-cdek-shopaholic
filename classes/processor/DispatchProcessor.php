<?php namespace Gabix\CdekShopaholic\Classes\Processor;

use Carbon\Carbon;
use CdekSDK\CdekClient;
use Gabix\CdekShopaholic\Classes\Helper\CdekHelper;
use Gabix\CdekShopaholic\Models\CdekDispatch;
use Lovata\OrdersShopaholic\Models\Order;
use Gabix\CdekShopaholic\Models\Settings;
use CdekSDK\Contracts\Response;
use CdekSDK\Requests\UpdateRequest;
use CdekSDK\Requests\DeliveryRequest;
use CdekSDK\Requests\DeleteRequest;
use Lovata\OrdersShopaholic\Models\OrderPosition;
use October\Rain\Support\Traits\Singleton;
use CdekSDK\Common;

/**
 * Class DispatchProcessor
 *
 * @package Gabix\CdekShopaholic\Classes\Processor
 */
class DispatchProcessor
{
    use Singleton;

    /** @var array */
    protected $arOrderData;

    /** @var null|Order */
    protected $obOrder = null;

    /** @var null|CdekDispatch */
    protected $obDispatch = null;

    /** @var CdekClient */
    protected $client;

    /**
     * Set CDEK client
     *
     * @param CdekClient $client
     *
     * @return self
     */
    public function setClient(CdekClient $client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * Set order model
     *
     * @param Order $obOrder
     *
     * @return self
     */
    public function setOrder(Order $obOrder)
    {
        $this->obOrder = $obOrder;
        $this->obDispatch = $obOrder->cdek;
        return $this;
    }

    /**
     * Delete remote dispatch
     *
     * @return bool
     * @throws \Exception
     */
    public function delete()
    {
        $request = DeleteRequest::create([
            'Number' => $this->obOrder->order_number,
        ]);

        $dispatchOrder = Common\Order::withDispatchNumber(
            $this->obDispatch->dispatch_number
        );

        $request->addOrder($dispatchOrder);

        $response = $this->client->sendDeleteRequest($request);

        $hasErrors = $response->hasErrors();

        if ($hasErrors) {
            return $this->checkErrors($response);
        }

        $this->obDispatch->dispatch_number = null;
        $this->obDispatch->sent_date = null;
        $this->obDispatch->status_code = null;
        $this->obDispatch->status_city = null;
        $this->obDispatch->modify_date = Carbon::now();
        $this->obDispatch->save();

        return true;
    }

    /**
     * Create or update dispatch via api
     *
     * @return bool
     * @throws \Exception
     */
    public function send()
    {
        $dispatchOrderData = $this->getDispatchOrderData();

        $dispatchOrder = new Common\Order($dispatchOrderData);

        $addressData = $this->getDispatchAddressData();
        $address = Common\Address::create($addressData);

        $dispatchOrder->setAddress($address);

        $package = CdekHelper::instance()->getPackageDimensions($this->obOrder->order_position, true);

        $package['Number'] = $this->obOrder->order_number;
        $package['BarCode'] = $this->obOrder->order_number;

        $package = Common\Package::create($package);

        foreach ($this->obOrder->order_position as $position) {
            $itemData = $this->getPackageItemData($position);
            $item = new Common\Item($itemData);
            $package->addItem($item);
        }

        $dispatchOrder->addPackage($package);

        if ($this->obDispatch->dispatch_number) {
            $request = UpdateRequest::create([
                'Number' => $this->obOrder->order_number,
            ]);
        }
        else {
            $request = DeliveryRequest::create([
                'Number' => $this->obOrder->order_number,
            ]);
        }

        $request->addOrder($dispatchOrder);

        $response = $this->client->sendDeliveryRequest($request);

        $hasErrors = $response->hasErrors();

        if ($hasErrors) {
            $this->checkErrors($response);
            return false;
        }

        $responseOrders = $response->getOrders();

        foreach ($responseOrders as $order) {
            return $this->processResponseOrder($order);
        }

        return false;
    }

    protected function getDispatchOrderData()
    {
        $recipientName = [];

        foreach(['name', 'last_name'] as $prop) {
            $nameField = (string) Settings::get('order_prop_'.$prop, $prop);
            if (!empty($this->obOrder->property[$nameField])) {
                $recipientName[] = $this->obOrder->property[$nameField];
            }
        }

        $recipientName = implode(' ', $recipientName);

        $senderId = (int) Settings::get('sender_id', 44);
        $senderCity = Common\City::create([
            'Code' => $senderId
        ]);
        $recCity = Common\City::create([
            'Code' => $this->obDispatch->city_id
        ]);

        $email = (string) Settings::get('order_prop_email', 'email');
        $email = $this->obOrder->property[$email] ?? '';

        $phone = (string) Settings::get('order_prop_phone', 'phone');
        $phone = $this->obOrder->property[$phone] ?? '';

        $data = [
            'Number' => $this->obOrder->order_number,
            'SendCity' => $senderCity,
            'RecCity' => $recCity,
            'RecipientName' => $recipientName,
            'RecipientEmail' => $email,
            'Phone' => $phone,
            'TariffTypeCode' => $this->obDispatch->tariff_id,
        ];

        return $data;
    }

    /**
     * Get order address data
     *
     * @return array
     */
    protected function getDispatchAddressData()
    {
        if (!empty($this->obOrder->shipping_type->property['show_pvz'])) {
            $data = ['PvzCode' => $this->obDispatch->pvz_code];
        } else {
            $data = [];
            foreach (['street', 'house', 'flat'] as $field) {
                $propName = (string) Settings::get('order_prop_' . $field, 'shipping_' . $field);
                $fieldName = ucfirst($field);
                $data[$fieldName] = $this->obOrder->property[$propName] ?? '';
            }
        }
        return $data;
    }

    /**
     * Get package item data for dispatch
     *
     * @param OrderPosition $position
     *
     * @return array
     */
    protected function getPackageItemData(OrderPosition $position)
    {
        $data = [
            'WareKey' => $position->offer->code,
            'Cost' => $position->price_value,
            'Payment' => 0,
            'Weight' => 100,
            'Amount' => $position->quantity,
            'Comment' => $position->offer->name,
        ];
        return $data;
    }

    /**
     * Update order data
     *
     * @param \Traversable|\CdekSDK\Common\Order $order
     *
     * @return bool
     */
    protected function processResponseOrder($order)
    {
        if ($this->obDispatch->dispatch_number) {
            $this->obDispatch->modify_date = Carbon::now();
        } else {
            $this->obDispatch->modify_date = null;
            $orderDate = $order->getDate();
            if ($orderDate) {
                $this->obDispatch->sent_date = Carbon::make($orderDate);
            }
            else {
                $this->obDispatch->sent_date = Carbon::now();
            }
        }
        $this->obDispatch->dispatch_number = $order->getDispatchNumber();
        $this->obDispatch->save();
        return true;
    }

    /**
     * @param Response $response
     *
     * @return bool
     * @throws \Exception
     */
    protected function checkErrors(Response $response)
    {
        $messages = $response->getMessages();
        foreach ($messages as $message) {
            $errorCode = $message->getErrorCode();
            if (!$errorCode) {
                continue;
            }
            $message = $message->getMessage();
            throw new \Exception($message);
        }
        return false;
    }
}
