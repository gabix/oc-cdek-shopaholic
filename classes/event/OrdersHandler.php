<?php namespace Gabix\CdekShopaholic\Classes\Event;

use Gabix\CdekShopaholic\Classes\CdekApiShipping;
use Gabix\CdekShopaholic\Models\CdekDispatch;
use Gabix\CdekShopaholic\Models\Settings;
use Lovata\OrdersShopaholic\Classes\Item\OrderItem;
use Lovata\OrdersShopaholic\Classes\Item\ShippingTypeItem;
use Lovata\OrdersShopaholic\Classes\Processor\OrderProcessor;
use Lovata\OrdersShopaholic\Controllers\Orders;
use Lovata\OrdersShopaholic\Models\Order;
use Lovata\OrdersShopaholic\Models\ShippingType;
use Lang;

/**
 * Class OrdersHandler
 *
 * @package Gabix\CdekShopaholic\Classes\Event
 */
class OrdersHandler
{
    /**
     * Add listeners
     *
     * @param \Illuminate\Events\Dispatcher $obEvent
     */
    public function subscribe($obEvent)
    {
        Orders::extend(function ($controller) {
            $controller->implement[]
                = '@Gabix.CdekShopaholic.Classes.Behaviors.CdekOrdersController';
        });

        $obEvent->listen(ShippingType::EVENT_GET_SHIPPING_TYPE_API_CLASS_LIST,
            function () {
                return [
                    CdekApiShipping::class => 'CDEK Api',
                ];
            });

        ShippingTypeItem::extend(function ($obItem) {
            /** @var ShippingTypeItem $obItem */
            $obItem->addDynamicProperty('api_result', null);
            $obItem->addDynamicMethod('setApiResult',
                function ($result) use ($obItem) {
                    $obItem->api_result = $result;
                });
        });

        $obEvent->listen(OrderProcessor::EVENT_ORDER_CREATED,
            function (Order $obOrder) {
                if (!empty($obOrder->property['city_id'])) {
                    try {
                        CdekDispatch::create([
                            'order_id' => $obOrder->id,
                            'city_id' => $obOrder->property['city_id'],
                            'pvz_code' => $obOrder->property['pvz_code'] ?? null,
                            'tariff_id' => $obOrder->property['shipping_tariff_id'] ?? null,
                            'region_id' => $cityData['regionId'] ?? null,
                            'country_id' => $cityData['countryId'] ?? null,
                            'country_iso' => $cityData['countryIso'] ?? null,
                            'city_data' => $obOrder->property['city_data'] ?: null
                        ]);
                    } catch (\Exception $e) {
                        // error
                    }
                }
            });

        Order::extend(function ($model) {
            /** @var Order $model */
            $model->hasOne['cdek'] = [CdekDispatch::class];
        });

        OrderItem::extend(function ($obItem) {
            /** @var OrderItem $obItem */
            $obItem->addDynamicMethod('getShippingLocation', function () use ($obItem) {
                /** @var CdekDispatch $cdek */
                $cdek = $obItem->getObject()->cdek;
                if ($cdek->pvz_code && $cdek->pvz) {
                    return $cdek->pvz->full_address.' ('.Lang::get('gabix.cdekshopaholic::lang.label.pvz').')';
                } else {
                    $return = ($cdek->city_data['name'] ? $cdek->city_data['name'] . ',' : '');
                    foreach (['street', 'house', 'flat'] as $field) {
                        $propName = (string) Settings::get('order_prop_'.$field, 'shipping_'.$field);
                        if (!empty($obItem->property[$propName])) {
                            $return .= ($field == 'flat' ? ', ' : ' ').$obItem->property[$propName];
                        }
                    }
                    return $return;
                }
            });
        });
    }
}
