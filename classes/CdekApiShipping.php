<?php namespace Gabix\CdekShopaholic\Classes;

use CdekSDK\Common\AdditionalService;
use Gabix\CdekShopaholic\Models\Pvz;
use Gabix\CdekShopaholic\Models\Settings;
use Lovata\Toolbox\Classes\Helper\PriceHelper;
use Gabix\CdekShopaholic\Classes\Helper\CdekHelper;
use Lovata\OrdersShopaholic\Classes\Item\ShippingTypeItem;
use Lovata\OrdersShopaholic\Classes\Processor\CartProcessor;
use Lovata\OrdersShopaholic\Interfaces\ShippingPriceProcessorInterface;
use Request;
use Lang;

/**
 * Class CdekApiShipping
 *
 * @package Gabix\CdekShopaholic\Classes
 */
class CdekApiShipping implements ShippingPriceProcessorInterface
{
    /** @var ShippingTypeItem */
    protected $shippingItem;

    /** @var CartProcessor */
    protected $cartProcessor;

    /** @var CdekHelper */
    protected $apiHelper;

    /** @var string */
    protected $error;

    /** @var array */
    protected $cached;

    /**
     * CdekApiShipping constructor.
     *
     * @param ShippingTypeItem $shippingItem
     */
    public function __construct(ShippingTypeItem $shippingItem)
    {
        $this->shippingItem = $shippingItem;
        $this->cartProcessor = CartProcessor::instance();
        $this->apiHelper = CdekHelper::instance();
    }

    /**
     * Get backend additional fields for shipping type form
     *
     * @return array
     */
    public static function getFields(): array
    {
        return [
            'property[show_pvz]' => [
                'type' => 'radio',
                'options' => [
                    '0' => 'gabix.cdekshopaholic::lang.field.show_pvz_no',
                    '1' => 'gabix.cdekshopaholic::lang.field.show_pvz_yes',
                ],
                'label' => 'gabix.cdekshopaholic::lang.field.show_pvz',
                'tab' => 'lovata.toolbox::lang.tab.settings'
            ],
            'property[tariff_ids]' => [
                'label' => 'gabix.cdekshopaholic::lang.field.tariff_ids',
                'span' => 'left',
                'comment' => 'gabix.cdekshopaholic::lang.field.tariff_ids_help',
                'commentHtml' => 'true',
                'tab' => 'lovata.toolbox::lang.tab.settings'
            ],
            'property[cost_addition]' => [
                'label' => 'gabix.cdekshopaholic::lang.field.cost_addition',
                'span' => 'left',
                'type' => 'number',
                'tab' => 'lovata.toolbox::lang.tab.settings'
            ],
        ];
    }

    /**
     * Returns true, if shipping fields has valid data
     *
     * @return bool
     */
    public function validate(): bool
    {
        $cityId = (int) Request::input('order.property.city_id');
        $cityData = (array) Request::input('order.property.city_data');
        $tariffId = (int) Request::input('order.property.shipping_tariff_id');
        $pvzCode = (string) Request::input('order.property.pvz_code');
        $showPvz = (bool) $this->shippingItem->getProperty('show_pvz');
        $tariffIds = (array) $this->getTariffIds();
        if(!$cityId || !$cityData) {
            $this->error = Lang::get('gabix.cdekshopaholic::lang.errors.no_city_selected');
        }
        elseif(!$tariffId || !in_array($tariffId, $tariffIds)) {
            $this->error = Lang::get('gabix.cdekshopaholic::lang.errors.wrong_tariff_selected');
        }
        elseif($showPvz && !$this->checkPickupPoint($pvzCode, $cityId)) {
            $this->error = Lang::get('gabix.cdekshopaholic::lang.errors.wrong_pvz_code');
        }
        else {
            return true;
        }
        return false;
    }

    /**
     * Calculate shipping price with using API
     *
     * @return float
     */
    public function getPrice(): float
    {
        $this->getData();

        return $this->cached['price'] ?? 0.0;
    }

    /**
     * Calculate shipping price with using API
     *
     * @return float|null
     */
    public function getMinPriceFormatted()
    {
        $price = $this->getPrice();
        if (!empty($this->cached['servicesPrice'])) {
            foreach ($this->cached['servicesPrice'] as $sCode => $sPrice) {
                $price -= $sCode !== AdditionalService::SERVICE_INSURANCE ? $sPrice : 0;
            }
        }

        return $price ? PriceHelper::format($price) : null;
    }

    /**
     * Get response message
     *
     * @return string
     */
    public function getMessage(): string
    {
        return $this->error;
    }

    /**
     * Get api data
     *
     * @param array $params
     *
     * @return array
     */
    public function getData(array $params = []): array
    {
        if (isset($this->cached)) {
            return $this->cached;
        }

        $this->cached = [];
        $cityId = (int) post('order.property.city_id');
        if (!empty($params['city_id'])) {
            $cityId = (int) $params['city_id'];
        }

        $tariffIds = $this->getTariffIds();
        if (!$tariffIds || !$cityId) {
            return [];
        }

        $sms_notify = (int) Settings::get('sms_notify');
        if (isset($params['sms_notify'])) {
            $sms_notify = (int) $params['sms_notify'];
        }

        if (!empty($params['total'])) {
            $total = (int) $params['total'];
        } else {
            $total = $this->cartProcessor->getCartPositionTotalPriceData()->price_value;
        }

        if (!empty($params['order'])) {
            $obPositionList = $params['order'];
        } else {
            $obPositionList = $this->cartProcessor->get();
        }

        $this->cached = $this->apiHelper->getShippingPriceData(
            $cityId,
            $tariffIds,
            $total,
            $obPositionList,
            $sms_notify
        );

        $iCostAddition = (int) $this->shippingItem->getProperty('cost_addition');

        $bUseCurrency = (bool) Settings::get('use_price_by_currency', false);
        if ($bUseCurrency && !empty($this->cached['priceByCurrency'])) {
            $this->cached['price'] = $this->cached['priceByCurrency'];
        } elseif (!empty($this->cached['currency'])) {
            $this->shippingItem->setActiveCurrency($this->cached['currency']);
        }

        if ($iCostAddition && $this->cached) {
            if ($this->cached['price'] == $this->cached['priceByCurrency']) {
                $this->cached['priceByCurrency'] += $iCostAddition;
            }
            $this->cached['price'] += $iCostAddition;
        }

        return $this->cached;
    }

    /**
     * Get tariffids by priority
     *
     * @return array
     */
    public function getTariffIds(): array
    {
        $tariffIds = $this->shippingItem->getProperty('tariff_ids');

        if ($tariffIds) {
            $tariffIds = explode(',', $tariffIds);
            $tariffIds = array_map('intval', $tariffIds);
            $tariffIds = array_filter($tariffIds);
        }

        return (array) $tariffIds;
    }

    /**
     * Check if pickup point exists
     *
     * @param string $pvzCode
     * @param int $cityCode
     *
     * @return bool
     */
    protected function checkPickupPoint($pvzCode, $cityCode): bool
    {
        $exists = Pvz::where([
            'active' => true,
            'code' => $pvzCode,
            'city_code' => $cityCode
        ])->first();

        return !!$exists;
    }
}
