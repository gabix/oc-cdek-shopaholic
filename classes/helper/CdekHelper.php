<?php namespace Gabix\CdekShopaholic\Classes\Helper;

use CdekSDK\Common\AdditionalService;
use CdekSDK\Responses\CalculationResponse;
use Gabix\CdekShopaholic\Classes\Processor\DispatchProcessor;
use Illuminate\Support\Collection;
use Lovata\OrdersShopaholic\Classes\Item\CartPositionItem;
use Lovata\Toolbox\Classes\Collection\ElementCollection;
use Gabix\CdekShopaholic\Models\Pvz;
use Gabix\CdekShopaholic\Models\Settings;
use October\Rain\Support\Traits\Singleton;
use CdekSDK\Requests\CalculationAuthorizedRequest;
use CdekSDK\CdekClient;
use Cache;
use GuzzleHttp\Client as HttpClient;
use Lovata\OrdersShopaholic\Models\Order;

/**
 * Class CdekHelper
 *
 * @package Gabix\CdekShopaholic\Classes\Helper
 */
class CdekHelper
{
    use Singleton;

    /** @var CdekClient */
    protected $client;

    /** @var array */
    protected $pvzCache = [];

    /** @var array */
    protected $dimensions = [];

    /**
     * Params used for dimensions calculation
     */
    public const DIMENSIONS_PARAMS = [
        'weight' => 'Weight',
        'length' => 'SizeA',
        'width'  => 'SizeB',
        'height' => 'SizeC',
    ];

    /**
     * Get value of active currency
     *
     * @return CdekClient
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Init currency data
     */
    protected function init()
    {
        $account = Settings::get('api_account');
        $secret = Settings::get('api_secret');
        $apiUrl = Settings::get('api_url');

        $httpClient = new HttpClient([
            'base_uri' => $apiUrl,
        ]);

        $this->client = new CdekClient($account, $secret, $httpClient);
    }

    /**
     * Get dimensions data from config
     *
     * @param Collection|ElementCollection $obPositionList
     * @param bool $isDispatch
     *
     * @return array
     */
    public function getPackageDimensions($obPositionList, bool $isDispatch = false)
    {
        foreach (self::DIMENSIONS_PARAMS as $sParam => $sDispatchParam) {
            $this->dimensions[$sParam] = 0;
            $this->dimensions[$sParam.'_max'] = 0;
        }
        $this->dimensions['volume'] = 0;
        $this->dimensions['side_max'] = 0;

        $sPackageVolumeMethod = Settings::get('package_volume_calculation');

        if (!$sPackageVolumeMethod) {
            $this->setDefaultDimensions();
            return $this->getFormattedDimensions($isDispatch);
        }

        foreach ($obPositionList as $obPositionItem) {
            $this->calculatePositionDimensions($obPositionItem);
        }

        if ($this->dimensions['volume'] && $sPackageVolumeMethod) {
            $iSideLen = pow($this->dimensions['volume'], 1/3);
            if ($iSideLen < $this->dimensions['side_max']) {
                $this->dimensions['length'] = $this->dimensions['side_max'];
                $this->dimensions['width'] = sqrt($this->dimensions['volume'] / $this->dimensions['side_max']);
                $this->dimensions['height'] = $this->dimensions['width'];
            } else {
                $this->dimensions['length'] = $iSideLen;
                $this->dimensions['width'] = $iSideLen;
                $this->dimensions['height'] = $iSideLen;
            }
        }

        $iWeightMultiplier = (float) Settings::get('package_weight_multiplier', 1);
        if ($iWeightMultiplier) {
            $this->dimensions['weight'] *= $iWeightMultiplier;
        }

        $iSideMultiplier = (float) Settings::get('package_side_multiplier', 1);
        if ($iSideMultiplier) {
            $this->dimensions['length'] *= $iSideMultiplier;
            $this->dimensions['width'] *= $iSideMultiplier;
            $this->dimensions['height'] *= $iSideMultiplier;
        }

        $this->setDefaultDimensions();

        return $this->getFormattedDimensions($isDispatch);
    }

    /**
     * Set default dimensions values from config
     * @param bool $isDispatch
     *
     * @return array
     */
    public function getFormattedDimensions($isDispatch = false)
    {
        $arReturn = [];
        foreach (self::DIMENSIONS_PARAMS as $sParam => $sDispatchParam) {
            $fValue = $this->dimensions[$sParam];
            if ($sParam != 'weight') {
                $fValue = (int) round($fValue, 0);
            } elseif ($isDispatch) {
                $fValue = (int) $fValue * 1000;
            }
            $arReturn[$isDispatch ? $sDispatchParam : $sParam] = $fValue;
        }
        return $arReturn;
    }

    /**
     * Set default dimensions values from config
     */
    public function setDefaultDimensions()
    {
        foreach (self::DIMENSIONS_PARAMS as $sParam => $sDispatchParam) {
            if (empty($this->dimensions[$sParam])) {
                $this->dimensions[$sParam] = (int) Settings::get('package_' . $sParam);
            }
        }
    }

    /**
     * @param $obPositionItem
     */
    protected function calculatePositionDimensions($obPositionItem)
    {
        $iVolume = (int) $obPositionItem->quantity;
        foreach (self::DIMENSIONS_PARAMS as $sParam => $sDispatchParam) {
            if ($obPositionItem instanceof CartPositionItem) {
                $iValue = (float) $obPositionItem->item->$sParam;
            } else {
                $iValue = (float) $obPositionItem->$sParam;
            }
            $this->dimensions[$sParam] += $iValue * (int) $obPositionItem->quantity;
            if ($sParam == 'weight') {
                continue;
            }
            if ($this->dimensions[$sParam.'_max'] < $iValue) {
                $this->dimensions[$sParam.'_max'] = $iValue;
            }
            if ($this->dimensions['side_max'] < $iValue) {
                $this->dimensions['side_max'] = $iValue;
            }
            $iVolume *= $iValue;
        }
        $this->dimensions['volume'] += $iVolume;
    }

    /**
     * Get cached shipping price data
     *
     * @param string   $cityGuid
     * @param float $total
     *
     * @return array
     */

    /**
     * Get cached shipping price data
     *
     * @param int   $cityId
     * @param array $tariffIds
     * @param float $total
     * @param bool  $smsNotify
     * @param Collection|ElementCollection $obPositionList
     *
     * @return array
     */
    public function getShippingPriceData(int $cityId, array $tariffIds, float $total, $obPositionList, $smsNotify = false)
    {
        $senderId = (int) Settings::get('sender_id', 44);
        $cacheMinutes = (int) Settings::get('cache_calc', 30);
        $addInsurance = (bool) Settings::get('add_insurance', true);
        $total = $addInsurance ? $total : 0;
        $tariffsImplode = implode('-', $tariffIds);

        $arDimensions = $this->getPackageDimensions($obPositionList, false);

        $cacheKey = [
            'CDEK_SHIPPING_CALC',
            $tariffsImplode,
            $cityId,
            $total,
            $smsNotify ? 1 : 0,
            implode('-', $arDimensions),
        ];

        $cacheKey = implode('_', $cacheKey);

        $cachedData = Cache::get($cacheKey);

        if (!$cachedData) {
            $cachedData = $this->calculateShipping($cityId, $senderId, $tariffIds, $total, $arDimensions, $smsNotify);

            Cache::put($cacheKey, $cachedData, $cacheMinutes);
        }

        return $cachedData;
    }

    /**
     * Calculate shipping price
     *
     * @param int   $cityId
     * @param int   $senderId
     * @param array $tariffIds
     * @param float $total
     * @param array $arDimensions
     * @param bool  $smsNotify
     *
     * @return array
     */
    protected function calculateShipping(int $cityId, int $senderId, array $tariffIds, float $total, array $arDimensions, $smsNotify = false)
    {
        $request = new CalculationAuthorizedRequest();

        $request->setSenderCityId($senderId)
            ->setReceiverCityId($cityId)
            ->addPackage($arDimensions);

        if ($total) {
            $request->addAdditionalService(AdditionalService::SERVICE_INSURANCE, $total);
        }

        if ($smsNotify) {
            $request->addAdditionalService(27);
        }

        $i = count($tariffIds) - 1;
        $priority = 1;

        for (;$i >= 0; $i--, $priority++) {
            $id = (int) trim($tariffIds[$i]);
            if (!$id) {
                continue;
            }
            $request->addTariffToList($id, $priority);
        }

        try {
            $response = $this->client->sendCalculationRequest($request);
            $hasErrors = $response->hasErrors();
        } catch (\Exception $e) {
            \Log::error('[CDEK API] error: '.$e->getMessage());
            $response = [];
        }

        if ($response && empty($hasErrors)) {
            return $this->processCalculationResponse($response);
        }

        $arErrors = $response->getErrors();
        if(!empty($arErrors[0])) {
            return [
                'error' => $arErrors[0]->getMessage()
            ];
        }

        return [];
    }

    /**
     * Process shipping data to array
     * @param CalculationResponse $response
     *
     * @return array
     */
    protected function processCalculationResponse(CalculationResponse $response)
    {
        $additionalServices = $response->getAdditionalServices();
        $services = [];
        foreach ($additionalServices as $service) {
            $serviceCode = $service->getServiceCode();
            $services[$serviceCode] = $service->getPrice();
        }
        $return = [
            'tariffId' => $response->getTariffId(),
            'price' => $response->getPrice(),
            'priceByCurrency' => $response->getPriceByCurrency(),
            'currency' => $response->getCurrency(),
            'deliveryPeriodMin' => $response->getDeliveryPeriodMin(),
            'deliveryPeriodMax' => $response->getDeliveryPeriodMax(),
            'servicesPrice' => $services,
        ];
        return $return;
    }

    /**
     * Delete dispatch via api
     *
     * @param Order $obOrder
     *
     * @return bool
     * @throws \Exception
     */
    public function deleteOrder(Order $obOrder)
    {
        return DispatchProcessor::instance()
            ->setClient($this->client)
            ->setOrder($obOrder)
            ->delete();
    }

    /**
     * Send dispatch data via api
     *
     * @param Order $obOrder
     *
     * @return bool
     * @throws \Exception
     */
    public function sendOrder(Order $obOrder)
    {
        return DispatchProcessor::instance()
            ->setClient($this->client)
            ->setOrder($obOrder)
            ->send();
    }

    /**
     * @param int $cityId
     *
     * @return array
     */
    public function getPvzList(int $cityId)
    {
        if ($cityId && !isset($this->pvzCache[$cityId])) {
            $this->pvzCache[$cityId] = Pvz::isActive()->byCity($cityId)->get()->toArray();

            usort($this->pvzCache[$cityId], function($a, $b){
                return strnatcasecmp($a['code'], $b['code']);
            });
        }

        return $cityId ? $this->pvzCache[$cityId] : [];
    }

    /**
     * Process and return post data
     *
     * @param array $data
     * @return array
     */
    public function getCityData($data)
    {
        $return = [];

        $fields = [
            "id"          => "int",
            "cityName"    => "string",
            "regionId"    => "int",
            "regionName"  => "string",
            "countryId"   => "int",
            "countryName" => "string",
            "countryIso"  => "string",
            "name"        => "string"
        ];

        foreach ($fields as $param => $type) {
            if ($type == 'int') {
                $return[$param] = (int) ($data[$param] ?? 0);
            }
            else {
                $return[$param] = strip_tags($data[$param] ?? '');
            }
        }

        $return['text'] = $return['name']; // select2 fix

        return $return;
    }
}
