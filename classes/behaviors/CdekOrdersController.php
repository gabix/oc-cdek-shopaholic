<?php namespace Gabix\CdekShopaholic\Classes\Behaviors;

use Flash;
use Lang;
use Event;
use Backend\Classes\ControllerBehavior;
use Gabix\CdekShopaholic\Classes\CdekApiShipping;
use Gabix\CdekShopaholic\Classes\Helper\CdekHelper;
use Gabix\CdekShopaholic\Models\CdekDispatch;
use Lovata\OrdersShopaholic\Classes\Item\ShippingTypeItem;
use Lovata\OrdersShopaholic\Models\Order;
use Lovata\OrdersShopaholic\Models\ShippingType;

/**
 * Class CdekOrdersController
 *
 * @package Gabix\CdekShopaholic\Classes\Behaviors
 */
class CdekOrdersController extends ControllerBehavior
{
    /**
     * Behavior constructor
     *
     * @param \Backend\Classes\Controller $controller
     *
     * @throws \ApplicationException
     */
    public function __construct($controller)
    {
        parent::__construct($controller);

        Event::listen('backend.form.beforeRefresh',
            array($this, 'handleShippingPrice'));
        Event::listen('backend.form.extendFields',
            array($this, 'addShippingFields'), 999);
    }

    public function onCdekDispatch()
    {
        $record_id = (int)post('record_id');
        $obOrder = Order::find($record_id);
        if (!$obOrder || empty($obOrder->cdek)) {
            Flash::error(Lang::get('gabix.cdekshopaholic::lang.api.invalid_order'));
            return [];
        }
        try {
            $action = post('action') == 'delete' ? 'deleteOrder' : 'sendOrder';
            if (!CdekHelper::instance()->$action($obOrder)) {
                Flash::error(Lang::get('gabix.cdekshopaholic::lang.api.error_message'));
                return [];
            }
            Flash::success(Lang::get('gabix.cdekshopaholic::lang.api.success_message'));
            return [
                '#cdek-actions' => $this->makePartial('~/plugins/gabix/cdekshopaholic/partials/_cdek_actions.htm',
                    [
                        'formModel' => $obOrder
                    ])
            ];
        } catch (\Exception $e) {
            Flash::error($e->getMessage());
            return [];
        }
    }

    /**
     * @param \Backend\Widgets\Form $obWidget
     */
    public function addShippingFields($obWidget)
    {
        if (!($obWidget->model instanceof Order) || $obWidget->isNested || $obWidget->context == 'create') {
            return;
        }
        $shippingTypes = ShippingType::active()->get();
        $typeIds = ['cdek' => [], 'pvz' => [], 'home' => []];
        foreach ($shippingTypes as $shippingType) {
            if ($shippingType->api_class != CdekApiShipping::class) {
                $typeIds['home'][] = $shippingType->id;
                continue;
            }
            $typeIds['cdek'][] = $shippingType->id;
            if (!empty($shippingType->property['show_pvz'])) {
                $typeIds['pvz'][] = $shippingType->id;
            } else {
                $typeIds['home'][] = $shippingType->id;
            }
        }

        if (!$obWidget->model->id || !$obWidget->model->cdek) {
            $obWidget->model->cdek = new CdekDispatch();
        }
        $shippingPrice = $obWidget->getField('full_shipping_price');
        if ($shippingPrice) {
            $shippingPrice->dependsOn = [
                'shipping_type',
                'cdek[city_id]',
                'order_offer'
            ];
        }
        foreach (['street', 'house', 'flat'] as $fieldName) {
            $field = $obWidget->getField('property[shipping_' . $fieldName . ']');
            if ($field) {
                $field->trigger = [
                    'action' => 'show',
                    'field' => 'shipping_type',
                    'condition' => 'value[' . implode('][', $typeIds['home']) . ']',
                ];
            }
        }
        $dependsOn = $shippingPrice ? $shippingPrice->dependsOn : [];
        $addFields = $this->getShippingFields($typeIds, $dependsOn);
        $obWidget->addTabFields($addFields);
    }

    /**
     * @param array $typeIds
     * @param array $dependsOn
     *
     * @return array
     */
    protected function getShippingFields($typeIds, $dependsOn)
    {
        return [
            'cdek[city_id]' => [
                'label' => 'gabix.cdekshopaholic::lang.field.city',
                'tab' => 'lovata.ordersshopaholic::lang.tab.shipping_address',
                'span' => 'left',
                'type' => 'partial',
                'path' => '~/plugins/gabix/cdekshopaholic/partials/_city_field.htm',
                'trigger' => [
                    'action' => 'show',
                    'field' => 'shipping_type',
                    'condition' => 'value[' . implode('][', $typeIds['cdek']) . ']',
                ],
            ],
            '_cdek_actions' => [
                'label' => null,
                'tab' => 'lovata.ordersshopaholic::lang.tab.shipping_address',
                'span' => 'right',
                'type' => 'partial',
                'path' => '~/plugins/gabix/cdekshopaholic/partials/_cdek_actions.htm',
                'trigger' => [
                    'action' => 'show',
                    'field' => 'shipping_type',
                    'condition' => 'value[' . implode('][', $typeIds['cdek']) . ']',
                ],
            ],
            'cdek[city_data]' => [
                'label' => null,
                'tab' => 'lovata.ordersshopaholic::lang.tab.shipping_address',
                'span' => 'left',
                'type' => 'partial',
                'cssClass' => 'hidden',
                'path' => '~/plugins/gabix/cdekshopaholic/partials/_city_data.htm'
            ],
            'cdek[pvz_code]' => [
                'label' => 'gabix.cdekshopaholic::lang.field.pvz',
                'tab' => 'lovata.ordersshopaholic::lang.tab.shipping_address',
                'span' => 'left',
                'emptyOption' => 'gabix.cdekshopaholic::lang.field.empty_pvz',
                'type' => 'dropdown',
                'dependsOn' => ['cdek[city_id]'],
                'trigger' => [
                    'action' => 'show',
                    'field' => 'shipping_type',
                    'condition' => 'value[' . implode('][', $typeIds['pvz']) . ']',
                ],
            ],
            'cdek[tariff_id]' => [
                'label' => 'gabix.cdekshopaholic::lang.field.tariff_id',
                'tab' => 'lovata.ordersshopaholic::lang.tab.shipping_address',
                'span' => 'left',
                'readOnly' => 'true',
                'dependsOn' => $dependsOn,
                'trigger' => [
                    'action' => 'show',
                    'field' => 'shipping_type',
                    'condition' => 'value[' . implode('][', $typeIds['cdek']) . ']',
                ],
            ]
        ];
    }

    /**
     * @param \Backend\Widgets\Form $obWidget
     * @param object                $dataHolder
     *
     * @return object
     */
    public function handleShippingPrice($obWidget, $dataHolder)
    {
        if ($obWidget->model instanceof Order
            && in_array('full_shipping_price', (array) post('fields'))
        ) {
            if (empty($dataHolder->data['cdek']['city_id'])
                || empty($dataHolder->data['shipping_type'])
            ) {
                return $dataHolder;
            }
            $shippingType
                = ShippingTypeItem::make($dataHolder->data['shipping_type']);
            if ($shippingType->api_class == CdekApiShipping::class) {
                $shippingData = $shippingType->api->getData([
                    'total' => $obWidget->model->position_total_price_data->price_value,
                    'order' => $obWidget->model->order_position,
                    'city_id' => $dataHolder->data['cdek']['city_id']
                ]);
                if (!empty($shippingData['price'])) {
                    $dataHolder->data['full_shipping_price']
                        = $shippingData['price'];
                    $dataHolder->data['cdek']['tariff_id']
                        = $shippingData['tariffId'];
                }
            }
        }
        return $dataHolder;
    }
}
