## CDEK Shipping integration for Shopaholic plugin / October CMS

Shipping method for Shopaholic that integrates CDEK delivery API in checkout and backend management.

## Disclaimer

As of current OctoberCMS Build 471 that uses outdated package of "doctrine/annotations" (1.4.0) this plugin can temporarily work `only with PHP versions from 7.1.x to 7.3.x`
We will update this plugin as soon as OctoberCMS 1.1 branch will come out.

## Plugin features

 - Pickup points select + map (Yandex.Maps)
 - Delivery city autocomplete 
 - Calculating delivery price based on configured tariffs
 - Checkout integration example for Shopaholic Bootstrap theme
 - CDEK Api in Shopaholic Shipping types
 - Backend delivery management in orders (dispatch send/update/delete)
 - Console commands for pickup points and orders delivery status synchronization

## Required Plugins

 - Toolbox
 - Shopaholic
 - Orders for Shopaholic

## Configuration

 - Enter into a contract with [CDEK Courier Company ](https://www.cdek.ru/clients/reglament.html) to obtain `Contractor identifier` (login) and `API Secret key`
 - You can use test accounts and API url as noted in [official documentation](https://confluence.cdek.ru/pages/viewpage.action?pageId=20264477), but limited tariffs will be available for usage
 - Fill the required params on plugin configuration page in backend, i.e. `yoursite.com/backend/system/settings/update/gabix/cdekshopaholic/settings`
 - Note that CDEK system needs to receive customer `Full name`, `Email` and `Phone` as well as `Street`, `House` and `Flat` (optional) for courier delivery, so corresponding `Order fields` under the self-titled tab are mandatory
 - Create needed Shipping types (`yoursite.com/backend/lovata/ordersshopaholic/shippingtypes`) based on [CDEK Tariff IDs](https://confluence.cdek.ru/pages/viewpage.action?pageId=20264477#DataExchangeProtocol(v1.5)-Appendix1.CDEKTariffsandDeliveryModes) you plan to use for delivery, selecting `CDEK Api` as `API method` and filling tariff ids by priority (don't forget the checkbox below for pickup types)
 - Setup cron artisan commands `php artisan shopaholic:cdek.pvz` (synchronize pickup points data) and `php artisan shopaholic:cdek.status` (update order delivery statuses)
 - Integrate delivery in frontend checkout, based on example for [Shopaholic Bootstrap theme](https://octobercms.com/theme/lovata-bootstrap-shopaholic) (see `themes/lovata-bootstrap-shopaholic` in plugin directory for corresponding/edited files)
 
Feel free to contact our support team if you have any questions regarding this integration.




## License

© 2019, [GABIX](https://gabix.studio) under Commercial License.
