<?php namespace Gabix\CdekShopaholic\Models;

use Model;

/**
 * Class Pvz
 *
 * @package Gabix\CdekShopaholic\Models
 *
 * @mixin \October\Rain\Database\Builder
 * @mixin \Eloquent
 *
 *
 * @property string                    $code
 * @property string                    $address
 * @property string                    $address_comment
 * @property string                    $allowed_cod
 * @property string                    $city
 * @property int                       $city_code
 * @property int                       $country_code
 * @property string                    $country_code_iso
 * @property string                    $country_name
 * @property string                    $email
 * @property string                    $full_address
 * @property bool                      $have_cashless
 * @property bool                      $is_dressing_room
 * @property string                    $metro_station
 * @property string                    $name
 * @property string                    $nearest_station
 * @property string                    $note
 * @property array                     $office_images
 * @property string                    $phone
 * @property string                    $postal_code
 * @property string                    $region_code
 * @property string                    $region_name
 * @property string                    $site
 * @property string                    $type
 * @property array                     $weight_limit
 * @property string                    $work_time
 * @property float                     $coord_x
 * @property float                     $coord_y
 * @property array                     $phone_details
 * @property array                     $work_times
 * @property bool                      $active
 *
 * @property \October\Rain\Argon\Argon $created_at
 * @property \October\Rain\Argon\Argon $updated_at
 *
 * @method static $this byCity(int $cityId)
 * @method static $this isActive()
 */
class Pvz extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'gabix_cdek_shopaholic_pvz';

    protected $primaryKey = 'code';

    public $incrementing = false;

    public $rules = [];

    /**
     * @var array
     */
    protected $casts = [
        'allowed_cod' => 'boolean',
        'have_cashless' => 'boolean',
        'is_dressing_room' => 'boolean',
        'active' => 'boolean',
    ];

    public $jsonable = [
        'office_images',
        'work_times',
        'weight_limit',
        'phone_details'
    ];

    public $fillable = [
        'address',
        'address_comment',
        'allowed_cod',
        'city',
        'city_code',
        'code',
        'country_code',
        'country_code_iso',
        'country_name',
        'email',
        'full_address',
        'have_cashless',
        'is_dressing_room',
        'metro_station',
        'name',
        'nearest_station',
        'note',
        'office_images',
        'phone',
        'postal_code',
        'region_code',
        'region_name',
        'site',
        'type',
        'weight_limit',
        'work_time',
        'coord_x',
        'coord_y',
        'phone_details',
        'work_times',
    ];

    /**
     * Active offices
     *
     * @param \Illuminate\Database\Eloquent\Builder|\October\Rain\Database\Builder $obQuery
     * @param \int                                                                 $cityId
     *
     * @return \Illuminate\Database\Eloquent\Builder|\October\Rain\Database\Builder;
     */
    public function scopeByCity($obQuery, $cityId)
    {
        return $obQuery->where('city_code', $cityId);
    }

    /**
     * Active offices
     *
     * @param \Illuminate\Database\Eloquent\Builder|\October\Rain\Database\Builder $obQuery
     *
     * @return \Illuminate\Database\Eloquent\Builder|\October\Rain\Database\Builder;
     */
    public function scopeIsActive($obQuery)
    {
        return $obQuery->where('active', 1);
    }
}
