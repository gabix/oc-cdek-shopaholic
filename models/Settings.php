<?php namespace Gabix\CdekShopaholic\Models;

use Model;
use Lovata\OrdersShopaholic\Models\Status;
use Lovata\OrdersShopaholic\Models\OrderProperty;

/**
 * Class Settings
 *
 * @package Gabix\CdekShopaholic\Models
 *
 * @mixin \October\Rain\Database\Builder
 * @mixin \Eloquent
 * @mixin \System\Behaviors\SettingsModel
 */
class Settings extends Model
{
    public $implement = [
        \System\Behaviors\SettingsModel::class
    ];

    // A unique code
    public $settingsCode = 'cdek_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';

    /**
     * Get options array
     *
     * @param string $fieldName
     * @param mixed  $value
     * @param        $formData
     *
     * @return array
     */
    public function getDropdownOptions($fieldName, $value, $formData)
    {
        if (in_array($fieldName, ['final_order_status_id', 'delivery_order_status_id'])) {
            return Status::orderBy('sort_order', 'asc')->get()->lists('name', 'id');
        } elseif (mb_substr($fieldName, 0, 11) == 'order_prop_') {
            return OrderProperty::orderBy('sort_order', 'asc')->get()->lists('name', 'code');
        }

        return [];
    }
}