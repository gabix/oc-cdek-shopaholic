<?php namespace Gabix\CdekShopaholic\Models;

use Lang;
use Model;

use Lovata\OrdersShopaholic\Models\Order;

use Gabix\CdekShopaholic\Classes\Helper\CdekHelper;

/**
 * Class CdekDispatch
 *
 * @package Gabix\CdekShopaholic\Models
 *
 * @mixin \October\Rain\Database\Builder
 * @mixin \Eloquent
 *
 * @property                                       $id
 * @property int                                   $order_id
 * @property int                                   $city_id
 * @property int                                   $tariff_id
 * @property string                                $pvz_code
 * @property int                                   $region_id
 * @property int                                   $country_id
 * @property string                                $country_iso
 * @property array                                 $city_data
 * @property bool                                  $sms_notify
 * @property string                                $dispatch_number
 * @property string                                $status_city
 * @property int                                   $status_code
 * @property \October\Rain\Argon\Argon             $sent_date
 * @property \October\Rain\Argon\Argon             $modify_date
 * @property \October\Rain\Argon\Argon             $status_date
 *
 * Relations
 * @property \Lovata\OrdersShopaholic\Models\Order $order
 * @property \Gabix\CdekShopaholic\Models\Pvz      $pvz
 */
class CdekDispatch extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'gabix_cdek_shopaholic_dispatch';

    public $rules = [];

    public $jsonable = ['city_data'];

    public $fillable = [
        'order_id',
        'city_id',
        'tariff_id',
        'pvz_code',
        'region_id',
        'country_id',
        'country_iso',
        'city_data',
        'sms_notify'
    ];

    public $timestamps = false;

    public $dates = ['sent_date', 'modify_date', 'status_date'];

    public $belongsTo = [
            'order' => [Order::class],
            'pvz'   => [Pvz::class, 'key' => 'pvz_code']
        ];

    /**
     * Before save method
     */
    public function beforeSave()
    {
        $arCityData = (array) $this->city_data;
        if (!empty($arCityData)) {
            $this->city_data = CdekHelper::instance()->getCityData($arCityData);
        } else {
            $this->city_data = [];
        }
        if(!$this->city_id) {
            $this->city_id = 0;
        }
        if(!$this->tariff_id) {
            $this->tariff_id = 0;
        }
        if(!$this->pvz_code) {
            $this->pvz_code = null;
        }
    }

    /**
     * Get pvz code options
     *
     * @return array
     */
    public function getPvzCodeOptions()
    {
        $arResult = Pvz::isActive()->byCity($this->city_id)->get()->lists('address', 'code');

        return $arResult;
    }

    /**
     * Return CDEk shipping label
     *
     * @return string
     */
    public function getStatusLabel()
    {
        $sResult = null;
        if (!empty($this->status_code)) {
            $sResult = Lang::get('gabix.cdekshopaholic::lang.status.status_'.$this->status_code);
            $sResult .= $this->status_city ? ', '.$this->status_city : '';
        }

        return  $sResult;
    }
}
